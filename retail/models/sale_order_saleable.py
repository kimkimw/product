from odoo import models, fields, api, _


class SaleOderSaleable(models.Model):
    _inherit = 'sale.order'

    def action_confirm(self):
        a = self.env['product.template'].sudo().search([(
            'id', '=', self.order_line.product_template_id.id)])
        qty_saleables = a.virtual_available - self.order_line.product_uom_qty
        qty_saleable = self.env['product.product'].sudo().search(
            [('id', '=', self.order_line.product_id.id)])
        qty_saleable.write({
            'qty_saleable': qty_saleables
        })
        return super(SaleOderSaleable, self).action_confirm()

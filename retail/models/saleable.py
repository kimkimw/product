from odoo import models, fields, api, _


class Saleable(models.Model):
    _inherit = 'product.product'

    qty_saleable = fields.Float(string='Số lượng có thể bán')
    # virtual_available = fields.Float(
    #     'Số lượng dự báo', search='_search_virtual_available',
    #     compute_sudo=False, digits='Product Unit of Measure')

    # def _search_virtual_available(self, operator, value):
    #     domain = [('virtual_available', operator, value)]
    #     product_variant_ids = self.env['product.product'].search(domain)
    #     return [('product_variant_ids', 'in', product_variant_ids.ids)]
